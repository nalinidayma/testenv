## testenv repo

This is a simple example of how to launch an apache/php app container that connects to a mysql db container running on your local machine,
using docker container images and simple docker-compose commands.
Get started by cloning this git repo.

---

## Pre-requisites
Should have either docker engine or docker-for-mac installed and running. 
(sorry not tested on windows, but as long as simple docker-compose or docker run commands work on your Win machine, this code should work.)
Must be able to connect to docker hub upstream registry to pull base images.
---

## Launching the stack: run ./launch_stack.sh
1. Clone this repo & cd to the directory testenv/
2. Run on command line: ./launch_stack.sh
You should see the below output if the stack starts up correctly:
Once started up, hit the browser with: "localhost:80/"
 
```
> ./launch_stack.sh 
Creating network "testenv_default" with the default driver
Building app
Step 1/2 : FROM php:5.6-apache
 ---> 9b87aacce8e4
Step 2/2 : RUN docker-php-ext-install mysqli
 ---> Using cache
 ---> 877451500ca5
Successfully built 877451500ca5
Creating testenv_db_1  ... done
Creating testenv_db_1  ... 
Creating testenv_app_1 ... done
Attaching to testenv_db_1, testenv_app_1
db_1   | [Entrypoint] MySQL Docker Image 5.7.21-1.1.3
db_1   | [Entrypoint] Initializing database
app_1  | AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.18.0.3. Set the 'ServerName' directive globally to suppress this message
app_1  | AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.18.0.3. Set the 'ServerName' directive globally to suppress this message
app_1  | [Mon Feb 26 07:16:32.262202 2018] [mpm_prefork:notice] [pid 1] AH00163: Apache/2.4.10 (Debian) PHP/5.6.33 configured -- resuming normal operations
app_1  | [Mon Feb 26 07:16:32.262677 2018] [core:notice] [pid 1] AH00094: Command line: 'apache2 -D FOREGROUND'
db_1   | [Entrypoint] Database initialized
db_1   | Warning: Unable to load '/usr/share/zoneinfo/iso3166.tab' as time zone. Skipping it.
db_1   | Warning: Unable to load '/usr/share/zoneinfo/leapseconds' as time zone. Skipping it.
db_1   | Warning: Unable to load '/usr/share/zoneinfo/tzdata.zi' as time zone. Skipping it.
db_1   | Warning: Unable to load '/usr/share/zoneinfo/zone.tab' as time zone. Skipping it.
db_1   | Warning: Unable to load '/usr/share/zoneinfo/zone1970.tab' as time zone. Skipping it.
db_1   | 
db_1   | [Entrypoint] running /docker-entrypoint-initdb.d/createtable.sql
db_1   | 
db_1   | 
db_1   | [Entrypoint] Server shut down
db_1   | 
db_1   | [Entrypoint] MySQL init process done. Ready for start up.
db_1   | 
db_1   | [Entrypoint] Starting MySQL 5.7.21-1.1.3

```
### You should now have an Apache container & MySQL DB container running on localhost:80 port, Mysql on localhost 3306 port. Launch the browser with "localhost:80/".
The browser should display random messages fetched from the DB (stored in a table called messages, created via createtable.sql file)
```
Message of the day...
========================
Keep calm n grab a beer...hopefully someone will fix the code in the meantime

```

## Deleting stack: run ./delete_stack.sh
1. To delete the stack, run from the command line: "./delete_stack.sh"
You should see below output:

```
> ./delete_stack.sh 
Removing testenv_app_1 ... done
Removing testenv_db_1  ... done
Removing network testenv_default
```

