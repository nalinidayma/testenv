USE database;
CREATE TABLE messages (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
message VARCHAR(128) NOT NULL 
);
INSERT INTO messages(id,message) VALUES (1,"If you think nobody cares if you are alive...try missing a car payment");
INSERT INTO messages(id,message) VALUES (2,"Dont worry be happy");
INSERT INTO messages(id,message) VALUES (3,"Keep calm n grab a beer...hopefully someone will fix the code in the meantime");
